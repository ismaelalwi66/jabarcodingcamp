<?php

    require('animal.php');
    include('ape.php');
    include('frog.php');

    $sheep = new Animal("shaun");

    echo "Nama : ".$sheep->name."<br>";
    echo "Jumlah Kaki : ".$sheep->legs."<br>";
    echo "Tipe darah dingin : ".$sheep->cold_blooded."<br><br>";

    $sungokong = new Ape("kera sakti");

    echo "Nama : ".$sungokong->name."<br>";
    echo "Jumlah Kaki : ".$sungokong->legs."<br>";
    echo "Tipe darah dingin : ".$sungokong->cold_blooded."<br>";
    $sungokong->yell();

    $kodok = new Frog("buduk");

    echo "Nama : ".$kodok->name."<br>";
    echo "Jumlah Kaki : ".$kodok->legs."<br>";
    echo "Tipe darah dingin : ".$kodok->cold_blooded."<br>";
    $kodok->jump();

?>
    